PK     �;oU�B�H         mimetypetext/x-wxmathmlPK     �;oUiQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     �;oU���24  24     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 20.12.1   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="40">

<cell type="code">
<input>
<editor type="input">
<line></line>
<line>kill(v,th,D,lam,D1);</line>
<line>D : sqrt(1-v*v)/(1+v*cos(th));</line>
<line>lam : sqrt(1-v*v);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o135)	">(%o135) </lbl><v>done</v><lbl altCopy="(%o136)	">(%o136) </lbl><f><r><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></r></f><lbl altCopy="(%o137)	">(%o137) </lbl><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>D1 : diff(D,th);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o138)	">(%o138) </lbl><f><r><fn><r><fnm>sin</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><h>·</h><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><e><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(sin(th)*v*sqrt(1-v^2))/(1-cos(th)*v)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o71)	">(%o71) </lbl><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><h>·</h><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><e><r><r><p><n>1</n><v>−</v><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v></p></r></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>v:0.7123;</line>
<line></line>
<line>th:0.9122;</line>
<line>sin(th);;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o102)	">(%o102) </lbl><n>0.7123</n><lbl altCopy="(%o103)	">(%o103) </lbl><n>0.9122</n><lbl altCopy="(%o104)	">(%o104) </lbl><n>0.7908520686513525</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sqrt(D*D*v*v-(lam-D)^2)/(D*v);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o139)	">(%o139) </lbl><f><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r><h>·</h><q><f><r><e><r><v>v</v></r><r><n>2</n></r></e><h>·</h><r><p><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></p></r></r><r><e><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r></r><r><n>2</n></r></e></r></f><v>−</v><e><r><r><p><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q><v>−</v><f><r><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></r></f></p></r></r><r><n>2</n></r></e></q></r><r><v>v</v><h>·</h><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>v;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o140)	">(%o140) </lbl><v>v</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(%);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o141)	">(%o141) </lbl><v>v</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>(1.403902849922785*(1-cos(th)*v)*sqrt((0.50737129*(1-v^2))/(1-cos(th)*v)^2-(sqrt(1-v^2)-sqrt(1-v^2)/(1-cos(th)*v))^2))/sqrt(1-v^2);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o106)	">(%o106) </lbl><n>0.7908520686513524</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(sin(th)*v*sqrt(1-v^2))/(1-cos(th)*v)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o78)	">(%o78) </lbl><v>−</v><n>1.242671757022413</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-sin(th)*(v/sqrt(1-v^2)*D^2);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o81)	">(%o81) </lbl><v>−</v><f><r><n>0.8025984931303289</n><h>·</h><r><p><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></p></r></r><r><e><r><r><p><n>1</n><v>−</v><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v></p></r></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(0.8025984931303289*(1-v^2))/(1-cos(th)*v)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o82)	">(%o82) </lbl><v>−</v><n>1.242671757022413</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>XD1: sqrt(D*D*v*v-(lam-D)^2)/(D*v) *(v/lam)*D^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o183)	">(%o183) </lbl><f><r><q><f><r><e><r><v>v</v></r><r><n>2</n></r></e><h>·</h><r><p><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></p></r></r><r><e><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r></r><r><n>2</n></r></e></r></f><v>−</v><e><r><r><p><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q><v>−</v><f><r><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></r></f></p></r></r><r><n>2</n></r></e></q></r><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ratsimp(%);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o185)	">(%o185) </lbl><f><r><q><r><p><e><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn></r><r><n>2</n></r></e><v>−</v><n>1</n></p></r><h>·</h><e><r><v>v</v></r><r><n>4</n></r></e><v>+</v><r><p><n>1</n><v>−</v><e><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn></r><r><n>2</n></r></e></p></r><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r><h>·</h><q><e><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn></r><r><n>2</n></r></e><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></q></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>sqrt((cos(th)^2-1)*v^4+(1-cos(th)^2)*v^2)/((cos(th)*v+1)^2);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o186)	">(%o186) </lbl><f><r><q><r><p><e><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn></r><r><n>2</n></r></e><v>−</v><n>1</n></p></r><h>·</h><e><r><v>v</v></r><r><n>4</n></r></e><v>+</v><r><p><n>1</n><v>−</v><e><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn></r><r><n>2</n></r></e></p></r><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><e><r><r><p><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>1</n></p></r></r><r><n>2</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(%);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o191)	">(%o191) </lbl><n>0.5499311374163133</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(XD1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o192)	">(%o192) </lbl><n>0.1644346094831831</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>kill(v,th,lam) ;</line>
<line>lam::sqrt(1-v*v);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o181)	">(%o181) </lbl><v>done</v><lbl altCopy="(%o182)	">(%o182) </lbl><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>v:0.83521;</line>
<line>th:0.9324;</line>
<line>lam:sqrt(1-v*v);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o188)	">(%o188) </lbl><n>0.83521</n><lbl altCopy="(%o189)	">(%o189) </lbl><n>0.9324</n><lbl altCopy="(%o190)	">(%o190) </lbl><n>0.5499311374163133</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(XD1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o176)	">(%o176) </lbl><n>0.1644346094831831</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(XD1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o166)	">(%o166) </lbl><n>0.1644346094831831</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(D1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o164)	">(%o164) </lbl><n>0.1644346094831831</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>trigreduce(%);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o145)	">(%o145) </lbl><v>−</v><f><r><q><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>th</v></p></r></fn><h>·</h><e><r><v>v</v></r><r><n>4</n></r></e><v>−</v><e><r><v>v</v></r><r><n>4</n></r></e><v>−</v><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>th</v></p></r></fn><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><e><r><v>v</v></r><r><n>2</n></r></e></q></r><r><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><h>·</h><q><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>th</v></p></r></fn><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><n>4</n><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>2</n></q><v>+</v><q><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>th</v></p></r></fn><h>·</h><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><e><r><v>v</v></r><r><n>2</n></r></e><v>+</v><n>4</n><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>th</v></p></r></fn><h>·</h><v>v</v><v>+</v><n>2</n></q></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line> </line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>v;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o119)	">(%o119) </lbl><n>0.7123</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(v^4-v^2)/(cos(th)^2*v^2-2*cos(th)*v+1);</line>
<line>D;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o124)	">(%o124) </lbl><n>0.785568348063976</n><lbl altCopy="(%o125)	">(%o125) </lbl><n>1.244311296740286</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(v^4-v^2)/(cos(th)*v-1)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o127)	">(%o127) </lbl><n>0.7855683480639757</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(v^4-v^2)/(lam/D-2)^2;</line>
<line>-(1.0*sqrt((0.50737129*(1-v^2))/(1-cos(th)*v)^2-(sqrt(1-v^2)-sqrt(1-v^2)/(1-cos(th)*v))^2))/(1-cos(th)*v);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o130)	">(%o130) </lbl><n>0.1212208572285897</n><lbl altCopy="(%o131)	">(%o131) </lbl><v>−</v><n>1.242671757022414</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>  D;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o132)	">(%o132) </lbl><n>1.244311296740286</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>lam;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o133)	">(%o133) </lbl><n>0.7018751384683746</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>lam/(1+v*cos(th));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o134)	">(%o134) </lbl><n>0.4887938478644083</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>-(sin(th)*v*sqrt(1-v^2))/(1-cos(th)*v)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o107)	">(%o107) </lbl><v>−</v><n>1.242671757022413</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>lam: ev(lam);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o114)	">(%o114) </lbl><n>0.7018751384683746</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>D:ev(D);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o117)	">(%o117) </lbl><n>1.244311296740286</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>(1-lam^2)*lam^2/(lam/D-2)^2;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o118)	">(%o118) </lbl><n>0.1212208572285897</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>ev(lam);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o111)	">(%o111) </lbl><n>0.7018751384683746</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>lam;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o112)	">(%o112) </lbl><q><n>1</n><v>−</v><e><r><v>v</v></r><r><n>2</n></r></e></q>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>f</line>
</editor>
</input>
</cell>

</wxMaximaDocument>PK      �;oU�B�H                       mimetypePK      �;oUiQ#4  4  
             5   format.txtPK      �;oU���24  24               �  content.xmlPK      �   �:    