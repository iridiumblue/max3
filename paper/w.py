import cachetools
import pickle
import code


try:
    with open('mycache_test.p', 'rb') as f:
        cache = pickle.load(f)
except FileNotFoundError:
    cache = {} # or cachetools.LRUCache(maxsize=5) or whatever you want

@cachetools.cached(cache)
def ok_then(a):
    code.interact(local=dict(globals(), **locals()))
    return a+2

ok_then(4)

with open('mycache_test.pickle', 'wb') as f:
    pickle.dump(cache, f)

print("Cache written out to disk.")