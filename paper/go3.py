import numpy as np
#import cupy as np
import math
import code
import sys
import matplotlib.pyplot as plt
import scipy.special
import code
from scipy.special import *
import mpmath as mp
import matplotlib.colors as mcol
import matplotlib.cm as cm
from matplotlib import collections  as mc
from collections import OrderedDict
from pprint import pprint
import cachetools
import _pickle as pickle
import signal
from sys import exit
import wavelen2rgb

back_door = False
cache={}

if False:
    try:
        with open('mycache.p', 'rb') as f:
            cache = pickle.load(f)
    except FileNotFoundError:
        print(': No cache found.')
        cache = {} # or cachetools.LRUCache(maxsize=5) or whatever you want
else:
    print("Skipping cache ........")

o_cache_len = len(cache)
print("Cache loaded with ", o_cache_len," frikkin items.")

def handler(signal_received, frame):
    if back_door : 
        exit(0)
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    write_cache(cache)
    exit(0)

signal.signal(signal.SIGTSTP, handler)



def write_cache(cache):
    if len(cache)>o_cache_len :
        print("writing out cache.")
        with open('mycache.p', 'wb') as f:
          pickle.dump(cache, f)
        print(' -- ')
    else :
        print('cache clean.')

g_lam = 20 #200
RESOLUTION = 400
#RESOLUTION = 100
#RESOLUTION = 75 

g_r0 = 3 + 1/(g_lam*g_lam - 1)


from wavelen2rgb import wavelen2rgb
  
def color_tuple_to_hex(rgb):
    s=[hex(o)[2:] for o in rgb]
    s=[(o if len(o)==2 else '0'+o) for o in s]
    return "#" + "".join(s)
    
def rainbow(sections):
   wvs = (380,780) 
   freq = np.linspace(1/wvs[1],1/wvs[0],sections)
   wavelengths = 1/freq
   colors = [wavelen2rgb(o) for o in wavelengths]
   colors = [color_tuple_to_hex(o) for o in colors]
   #hxcolors = [hex(o)[2:] for o in colors]
   #code.interact(local=dict(globals(), **locals())) 
   return colors

NUM_COLORS=800
SPECTRUM = rainbow(NUM_COLORS)

def freq(th,lam):
    ret = 1/ (lam-np.cos(th)*np.sqrt(lam*lam-1))
    return ret

def _psi(r0,r,b):
    #try:
        r1,r2,r3 = dem_roots(b)
        ret = 2*mp.ellipf(
            np.arcsin(p_sqrt((r3 - 1/r)/(r3 - r2))), 
            (r2 - r3)/(r1 - r3)) *(
                (p_sqrt((r*r1 - 1)/(r*(r1 - r3))))*p_sqrt((r*r2 - 1)/(r*(r2 - r3)))*(r*r3 - 1)/(p_sqrt(1/b**2 + (2 - r)/r**3)*r*np.sqrt((1 - r*r3)/(r*(r2 - r3)))))
        #if np.abs(ret.imag) > 1e-8 : 
        #    print("Warning - complex part : *_*peri(b) = ", ret)
        return float(complex(ret).real)
    #except:
    #    print("Psi error.")
    #   return 0

# Omega = angle at infinity

def psi(r0,r,b):
    
    #print('Roots : ', r1,r2,r3)
    #code.interact(local=dict(globals(), **locals()))
    ret = _psi(r0,r,b) - _psi(r0,r0,b)
    if np.abs(ret.imag) > 1e-8 : 
        print("Warning - complex part : peri(b) = ", ret)
    
    return float(complex(ret).real) # export from mpf

def p_sqrt(x):
  #print(' >', x)
  t=mp.sqrt(x)
  #print(' <', t)
  return complex(t)
  
def peri(b):
    a = (-9*b*b+np.sqrt(3)*p_sqrt(27*b**4 - b**6))**(1/3.0)
   
    ret = a/(3**(2/3)) + b*b/(3**(1/3)*a)
    if ret.imag > 1e-8 : 
        print("Warning - complex part : peri(b) = ", ret)
    return complex(ret).real

def dem_roots(b):
  
   t= (-54 *b**4 + b**6 + 6 * p_sqrt(3) * p_sqrt(27 * b**8 - b**10))
   q1 = t**(1.0/3) if t.real > 0 else -(-t)**(1/3)  # Python arithmetic glitch

   r1 = 1.0/6 *(1 + b**2/q1 + q1/b**2)

   r2 = 1.0/6 - ((1 - p_sqrt(-3))*b**2)/(12*q1)-((1+p_sqrt(-3))*q1)/(12*b**2)

   r3 = 1.0/6 - ((1 + p_sqrt(-3))*b**2)/(12*q1)-((1-p_sqrt(-3))*q1)/(12*b**2)
   #print('dem roots ....')
 
   return r1,r2,r3


#Cache me outside how bad dat

def omega_prime_r(x,r):
     
    epsilon=1e-7

    #if x + epsilon >= maxi : return 0

    y1=omega_r(x,r)
    y2=omega_r(x+epsilon,r) 
    ret = (y2-y1)/epsilon
    #print(
    #     "Calculating intensity ... ",y1,y2,ret,epsilon)
    if ret<0:
       print("Oops, ret negative")
      
    return ret

def omega_p(th_emit) :
    return omega_prime_r(th_emit,1E11)


@cachetools.cached(cache)
def omega_r(th_emit,r):
    
    flip_sign = np.sign(th_emit); th_emit = np.abs(th_emit)
    EMIT_MIN=1e-4
    b = g_r0/np.sqrt(1 - 2/g_r0)*np.sin(th_emit)
    if th_emit < EMIT_MIN :
        return 0
    if th_emit < np.pi/2 :
        return float(psi(g_r0,r,b) * flip_sign)
    else :
        print("At ye! ")
        
        if  True: #b > 3 * np.sqrt(3) :
            # verify also that b = g_r0/  np.sqrt(1 - 2/g_r0) # angle is now tangent
            
            p = peri(b) # can now be considered to be a tangent emitter at r=p, th=pi/2
            
            # ?_psi(p,100000,1) + _psi(p,g_r0,1)
            a1 = _psi(p,r,b)
            a2 = _psi(p,g_r0,b)
            ret = a1+a2
            if np.abs(ret.imag) > 1e-8 : 
                print("Omega : Warning, imaginary part : ", ret)
            return float(ret.real * flip_sign)
        else : 
            print("Out of range.")
            return 1e10 * flip_sign
        return 0   

def omega(th_emit):
    return omega_r(th_emit,1e11)

def inv_pred(f,k):
    k * f*f * np.sqrt(1 - (g_lam-1/ff)**2 )

def get_k(f,f_p,a):
    k = f_p / (a/f*f * np.sqrt( (g_lam-1/ff)**2 - 1  ))
    return k

th= 0.30
om=omega(th)
om_p = omega_p(th)
a=1/om_p
eps=1E-11
th_2 = th + eps 
om_2 = omega(th+eps)
ff = freq(th,g_lam)
f_prime = (freq(th_2,g_lam)-freq(th,g_lam)) / (om_2-om)

f_e_prime = (freq(th_2,g_lam)-freq(th,g_lam))/(th_2 - th)

ff*ff*np.sqrt(g_lam*g_lam-1-(g_lam-1/ff)**2)
f_prime
a*ff*ff*np.sqrt(g_lam*g_lam-1-(g_lam-1/ff)**2)


get_k(ff,f_e_prime,a)

  
#fr = [freq(o, g_lam) for o in th]


code.interact(local=dict(globals(), **locals()))

#th  = np.arange(0.005,np.pi/2-0.0001,0.02) 
#th = np.concatenate(( th, np.arange(np.pi/2-0.3,np.pi/2-0.0001,0.0002) ) )
#om = [omega(o) for o in th]
#om_p = [omega_p(o) for o in th]

#plt.plot(th,om)
#plt.plot(th,om_p)


#plt.show()